
## My tarsnap helper scripts.

### tarsnap_completion

A set of bash completions. Either drop it in your equivalent of
/etc/bash_completion.d/, or source it some other way.

###  check_tarsnap

Nagios plugin which works by reading a text file containing a Unix
timestamp of the most recent tarsnap backup. See the top comment in
check_tarsnap for an example on how your tarsnap backup script can
generate such a file.

### tarsnap_usage

This Munin plugin keeps track on how much data a machine has backed up
on tarsnap. This is done by reading the current amount of data from a
text file, preferably updated by the tarsnap backup script. See the
plugin documentation for details.
